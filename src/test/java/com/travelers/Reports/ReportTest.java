package com.travelers.Reports;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;

public class ReportTest {
    public static void main(String[] args) {
        ExtentSparkReporter reporter = new ExtentSparkReporter("com/travelers/Reports");
        ExtentReports reports = new ExtentReports();
        reports.attachReporter(reporter);

        ExtentTest test = reports.createTest("Our first test");
        test.log(Status.FAIL, "Fail");
        test.log(Status.PASS, "Pass");
        test.log(Status.INFO, "Info");
        test.log(Status.WARNING, "Warning");
        test.log(Status.SKIP, "Skip");
        test.pass("Test is ok!!!");

        reports.flush();


    }
}
