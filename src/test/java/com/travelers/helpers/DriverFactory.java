package com.travelers.helpers;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;

public class DriverFactory {
    private static WebDriver driverInstance;

    public static WebDriver getDriver(DriverType driverType) {
        if (driverInstance == null) {
            getSpecificDriver(driverType);
            String driverPath = "/Users/joans/IdeaProjects/SeleniumAutomationPractiseJava/src/main/resources/Executables/Drivers/chromedriver";
            System.setProperty("webdriver.chrome.driver", driverPath);
            driverInstance.manage().window().maximize();
        }
        return driverInstance;
    }

    private static void getSpecificDriver(DriverType driverType) {
        switch (driverType) {
            case IE:
                String microsoftEdgeDriverPath = "/Users/joans/IdeaProjects/SeleniumAutomationPractiseJava/src/main/resources/Executables/Drivers/msedgedriver";
                System.setProperty("webdriver.edge.driver", microsoftEdgeDriverPath);
                driverInstance = new EdgeDriver();
                break;
            case CHROME:
                String chromeDriverPath = "/Users/joans/IdeaProjects/SeleniumAutomationPractiseJava/src/main/resources/Executables/Drivers/chromedriver";
                System.setProperty("webdriver.chrome.driver", chromeDriverPath);
                driverInstance = new ChromeDriver();
                break;
            case FIREFOX:
                String firefoxDriverPath = "/Users/joans/IdeaProjects/SeleniumAutomationPractiseJava/src/main/resources/Executables/Drivers/geckodriver";
                System.setProperty("webdriver.gecko.driver", firefoxDriverPath);
                FirefoxOptions options = new FirefoxOptions();
                driverInstance = new FirefoxDriver(options);
                break;
            default:
                System.out.println("Lack of proper driver");

        }
    }

    public static void resetDriver() {
        driverInstance = null;
    }
}
