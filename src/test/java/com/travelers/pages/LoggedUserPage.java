package com.travelers.pages;

import com.travelers.helpers.SeleniumHelper;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoggedUserPage {

    @FindBy(xpath = "//*[@id='body-section']/div[1]/div/div/div[1]/h3")
    private WebElement heading;

    private final SeleniumHelper helper;

    public LoggedUserPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.helper = new SeleniumHelper(driver);
    }

    public String getHeadingText() {
        return heading.getText();
    }
}
