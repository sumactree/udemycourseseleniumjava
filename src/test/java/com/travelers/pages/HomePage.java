package com.travelers.pages;

import com.travelers.helpers.SeleniumHelper;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;


public class HomePage {

    @FindBy(xpath = "//span[text() = 'Search by Hotel or City Name']")
    private WebElement SearchSpan;

    @FindBy(xpath = "//*[@id='select2-drop']/div/input")
    private WebElement SearchCityInput;

    @FindBy(name = "checkin")
    private WebElement CheckIn;

    @FindBy(name = "checkout")
    private WebElement CheckOut;

    @FindBy(id = "travellersInput")
    private WebElement PeopleCounter;

    @FindBy(id = "adultPlusBtn")
    private WebElement AddAdults;

    @FindBy(id = "adultMinusBtn")
    private WebElement MinusAdults;

    @FindBy(id = "childPlusBtn")
    private WebElement AddChild;

    @FindBy(id = "childMinusBtn")
    private WebElement MinusChild;

    @FindBy(xpath = "//*[@id='hotels']/form/div[5]/button")
    private WebElement searchFlights;

    @FindBy(xpath = "//*[@id='select2-drop']/ul/li/ul/li/div")
    private WebElement selectCity;

    @FindBy(xpath = "//*[@id='li_myaccount']/a")
    private List<WebElement> myAccount;

    @FindBy(xpath = "//*[@id='li_myaccount']/ul/li[2]/a")
    private List<WebElement> signUp;

    private final SeleniumHelper helper;

    private static final Logger logger = LogManager.getLogger();

    public HomePage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.helper = new SeleniumHelper(driver);
    }

    public void typeCity(String CityName) {
        logger.info("Setting City " + CityName);

        SearchSpan.click();
        SearchCityInput.sendKeys(CityName);
        helper.waitingForElement(selectCity);
        SearchCityInput.sendKeys(Keys.ENTER);
        logger.info("Setting City done");
    }

    public void typeDateRange(String CheckInDate, String CheckOutDate) {
        logger.info("Setting dates check-in: " + CheckInDate + "check-out" + CheckOutDate);
        CheckIn.sendKeys(CheckInDate);
        CheckOut.sendKeys(CheckOutDate);
        logger.info("Setting dates done");
    }

    public void typeNumberOfPeople() {
        logger.info("Adding adults adults and kids");
        helper.waitingForElement(PeopleCounter);
        PeopleCounter.click();
        helper.waitingForElement(AddAdults);
        AddAdults.click();
        AddChild.click();
        AddChild.click();
        logger.info("Adding travellers done");
    }

    public void searchingFlights() {
        logger.info("Performing search");
        searchFlights.click();
        logger.info("Performing search done");
    }

    public void openFormSignIn() {
        myAccount.stream().filter(WebElement::isDisplayed).findFirst().ifPresent(WebElement::click);
        signUp.stream().filter(WebElement::isDisplayed).findFirst().ifPresent(WebElement::click);
    }
}
