package com.travelers.pages;

import com.travelers.helpers.SeleniumHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.ArrayList;
import java.util.List;

public class ResultsHotelsPage {

    @FindBy(xpath = "//*[@id='body-section']/div[5]/div/div[3]/div[1]/div/table")
    private WebElement tableOfHotelNames;

    @FindBy(xpath = "//*[@id='body-section']/div[5]/div/div[3]/div[1]/div/table")
    private WebElement tableOfHotelPrices;

    @FindBy(xpath = "//*[@id=\'body-section\']/div[5]/div[1]/div[3]/div/div/h2")
    private WebElement WrongResultsTest;

    private final SeleniumHelper helper;

    public ResultsHotelsPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.helper = new SeleniumHelper(driver);
    }

    public List<String> getHotelNames() {
        List<String> hotelNames = new ArrayList<>();
        helper.waitForListSize(tableOfHotelNames.findElements(By.xpath(".//h4//b")));
        List<WebElement> hotelNameWebelements = tableOfHotelNames.findElements(By.xpath("//h4//b"));
        for (WebElement hotelNameWebelement : hotelNameWebelements) {
            System.out.println(hotelNameWebelement.getText());
            hotelNames.add(hotelNameWebelement.getText());
        }
        return hotelNames;
    }
//Another way to write a getOfPricies class :
//        public List<String> getOfHotelPrices() {
//        List<String> hotelPrices = new ArrayList<>();
//            List<WebElement> hotelPriceWebelements = tableOfHotelPrices.findElements(By.xpath("//div[contains(@class,'price_tab')]//b"));
//            for(WebElement hotelPriceWebelement: hotelPriceWebelements) {
//                System.out.println(hotelPriceWebelement.getText());
//                hotelPrices.add(hotelPriceWebelement.getText());
//
//            }
//            return hotelPrices;

    public List<String> getOfHotelPrices() {
        List<String> hotelPrices = new ArrayList<>();
        List<WebElement> hotelPriceWebelements = tableOfHotelPrices.findElements(By.xpath("//div[contains(@class,'price_tab')]//b"));
        hotelPriceWebelements.forEach(hotelPrice -> hotelPrices.add(hotelPrice.getText()));
        for (String name : hotelPrices) {
            System.out.println(name);
        }
        return hotelPrices;

    }

}
