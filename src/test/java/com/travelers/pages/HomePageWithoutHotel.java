package com.travelers.pages;

import com.travelers.helpers.SeleniumHelper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class HomePageWithoutHotel {

    @FindBy(name = "checkin")
    private WebElement CheckIn;

    @FindBy(name = "checkout")
    private WebElement CheckOut;

    @FindBy(id = "travellersInput")
    private WebElement PeopleCounter;

    @FindBy(id = "adultPlusBtn")
    private WebElement AddAdults;

    @FindBy(id = "adultMinusBtn")
    private WebElement MinusAdults;

    @FindBy(id = "childPlusBtn")
    private WebElement AddChild;

    @FindBy(id = "childMinusBtn")
    private WebElement MinusChild;

    @FindBy(xpath = "//*[@id='hotels']/form/div[5]/button")
    private WebElement searchFlights;

    private final SeleniumHelper helper;

    private static final Logger logger = LogManager.getLogger();

    public HomePageWithoutHotel(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.helper = new SeleniumHelper(driver);
    }

    public void typeDateRange(String CheckInDate, String CheckOutDate) {
        logger.info("Setting dates check-in: " + CheckInDate + "check-out" + CheckOutDate);
        CheckIn.sendKeys(CheckInDate);
        CheckOut.sendKeys(CheckOutDate);
        logger.info("Setting dates done");
    }

    public void typeNumberOfPeopleWithOneChildTwoAdults() {
        logger.info("Adding adults and kids");
        helper.waitingForElement(PeopleCounter);
        PeopleCounter.click();
        helper.waitingForElement(AddAdults);
        AddAdults.click();
        AddChild.click();
        AddChild.click();
        AddChild.click();
        logger.info("Adding travellers done");
    }

    public void searchingFlights() {
        logger.info("Performing search");
        searchFlights.click();
        logger.info("Performing search done");
    }
}
