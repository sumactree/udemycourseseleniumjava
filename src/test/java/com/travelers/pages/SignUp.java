package com.travelers.pages;

import com.travelers.helpers.SeleniumHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;
import java.util.stream.Collectors;

public class SignUp {
    @FindBy(name = "firstname")
    private WebElement firstName;

    @FindBy(name = "lastname")
    private WebElement lastName;

    @FindBy(name = "phone")
    private WebElement phoneNumber;

    @FindBy(name = "email")
    private WebElement email;

    @FindBy(name = "password")
    private WebElement password;

    @FindBy(name = "confirmpassword")
    private WebElement confirmPassword;

    @FindBy(xpath = "//*[@id='headersignupform']/div[9]/button")
    private WebElement confirmButton;

    @FindBy(xpath = "//*[@id='headersignupform']/div[2]/div//p")
    private List<WebElement> errors;

    @FindBy(xpath = "/html/body/nav/div/div[2]/ul[2]/ul/li[1]/a")
    private WebElement myAccount;

    @FindBy(xpath = "/html/body/nav/div/div[2]/ul[2]/ul/li[1]/ul/li[2]/a")
    private WebElement logOut;

    private final SeleniumHelper helper;

    public SignUp(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.helper = new SeleniumHelper(driver);
    }

    public void setFirstName(String firstNameValue) {
        firstName.sendKeys(firstNameValue);
    }

    public void setLastName(String lastNameValue) {
        lastName.sendKeys(lastNameValue);
    }

    public void setPhoneNumber(String phoneNumberValue) {
        phoneNumber.sendKeys(phoneNumberValue);
    }

    public void setEmail(String emailValue) {
        email.sendKeys(emailValue);
    }

    public void setPassword(String PasswordValue) {
        password.sendKeys(PasswordValue);
    }

    public void setConfirmPassword(String ConfirmPasswordValue) {
        confirmPassword.sendKeys(ConfirmPasswordValue);
    }

    public void clickConfirmPassword() {
        confirmButton.click();
    }

    public List<String> takeErrors() {
        return errors.stream().map(WebElement::getText).collect(Collectors.toList());
    }

    public void clickMyAccount() {
        myAccount.click();
    }

    public void clickLogOut() {
        logOut.click();
    }
}
