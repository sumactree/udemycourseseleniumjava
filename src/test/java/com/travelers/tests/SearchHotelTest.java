package com.travelers.tests;

import com.aventstack.extentreports.ExtentTest;
import com.travelers.helpers.Listener;
import com.travelers.helpers.dataFromExcel;
import com.travelers.helpers.SeleniumHelper;
import com.travelers.pages.HomePage;
import com.travelers.pages.ResultsHotelsPage;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;
import java.util.List;

@Listeners(Listener.class)
public class SearchHotelTest extends BaseSeleniumTest {
    @Test(dataProvider = "getData")
    public void searchHotelTest(String CityName, String checkInDate, String checkOutDate, String fHotel, String fPrice, String sHotel, String sPrice, String tHotel, String tPrice, String foHotel, String foPrice) throws IOException {
        ExtentTest test = reports.createTest("Search Hotel Test");
        HomePage homePage = new HomePage(driver);

        test.info("On the Home Page", getScreenshot());
        homePage.typeCity(CityName);
        homePage.typeDateRange(checkInDate, checkOutDate);
        homePage.typeNumberOfPeople();
        String infoText = "Before performing search with CityName %s, checkInDate %s, checkOutDate %s";
        test.info(String.format(infoText, CityName, checkInDate, checkOutDate), getScreenshot());
        homePage.searchingFlights();

        test.info("Checking hotel name and prices", getScreenshot());
        ResultsHotelsPage resultPage = new ResultsHotelsPage(driver);
        List<String> hotelNames = resultPage.getHotelNames();
        SeleniumHelper.takeScreenshot(driver);
        Assert.assertEquals(fHotel, hotelNames.get(0));
        Assert.assertEquals(sHotel, hotelNames.get(1));
        Assert.assertEquals(tHotel, hotelNames.get(2));
        Assert.assertEquals(foHotel, hotelNames.get(3));

        List<String> hotelPrices = resultPage.getOfHotelPrices();
        Assert.assertEquals(hotelPrices.get(0), fPrice);
        Assert.assertEquals(hotelPrices.get(1), sPrice);
        Assert.assertEquals(hotelPrices.get(2), tPrice);
        Assert.assertEquals(hotelPrices.get(3), foPrice);
    }

    @DataProvider
    public Object[][] getData() {
        Object[][] data = null;
        try {
            data = dataFromExcel.readExcelData(new File("src//main//resources//Files//Data.xlsx"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return data;
    }


}
