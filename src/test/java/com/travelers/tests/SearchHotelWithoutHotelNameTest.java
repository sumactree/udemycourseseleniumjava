package com.travelers.tests;

import com.aventstack.extentreports.ExtentTest;
import com.travelers.helpers.Listener;
import com.travelers.helpers.SeleniumHelper;
import com.travelers.helpers.dataFromExcelWithoutHotel;
import com.travelers.pages.HomePageWithoutHotel;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;

@Listeners(Listener.class)
public class SearchHotelWithoutHotelNameTest extends BaseSeleniumTest {
    @Test(dataProvider = "getData")
    public void searchWithoutHotelTest(String CityName, String checkInDate, String checkOutDate, String fHotel, String fPrice, String sHotel, String sPrice, String tHotel, String tPrice, String foHotel, String foPrice) throws IOException {

        ExtentTest test = reports.createTest("Search without Hotel Test");
        HomePageWithoutHotel homePageWithoutHotel = new HomePageWithoutHotel(driver);

        test.info("On the Home Page", getScreenshot());
        homePageWithoutHotel.typeDateRange(checkInDate, checkOutDate);
        homePageWithoutHotel.typeNumberOfPeopleWithOneChildTwoAdults();
        String infoText = "Before performing search with checkInDate %s, checkOutDate %s";
        test.info(String.format(infoText, checkInDate, checkOutDate), getScreenshot());
        homePageWithoutHotel.searchingFlights();

        SeleniumHelper.takeScreenshot(driver);
        WebElement resultWrongPage = driver.findElement(By.xpath("//*[@id='body-section']/div[5]/div[1]/div[3]/div/div/h2"));
        Assert.assertEquals(resultWrongPage.getText(), "No Results Found");

    }

    @DataProvider
    public Object[][] getData() {
        Object[][] data = null;
        try {
            data = dataFromExcelWithoutHotel.readExcelData(new File("src//main//resources//Files//Data.xlsx"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return data;
    }


}
