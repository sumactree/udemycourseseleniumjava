package com.travelers.tests;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.MediaEntityModelProvider;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import com.travelers.helpers.DriverFactory;
import com.travelers.helpers.DriverType;
import com.travelers.helpers.SeleniumHelper;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

public abstract class BaseSeleniumTest {
    //        If we want to control the size of window
//        Dimension dimension = new Dimension(1600, 800);
//        driver.manage().window().setSize(dimension);
    protected WebDriver driver;
    protected ExtentSparkReporter reporter;
    protected ExtentReports reports;

    @BeforeTest
    public void SetUpReport() {
        reporter = new ExtentSparkReporter("src/main/resources/Reports");
        reports = new ExtentReports();
        reports.attachReporter(reporter);
    }

    @BeforeMethod
    public void SetUp() {
        driver = DriverFactory.getDriver(DriverType.CHROME);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("http://www.kurs-selenium.pl/demo/");

    }

    @AfterMethod
    public void tearDown() {
        driver.quit();// close all windows
        DriverFactory.resetDriver();
//        driver.close(); // close only first window
//        }
    }

    @AfterTest
    public void TearDownReporter() {
        reports.flush();
    }

    protected MediaEntityModelProvider getScreenshot() throws IOException {
        return MediaEntityBuilder.createScreenCaptureFromPath(SeleniumHelper.takeScreenshot(driver)).build();
    }

}
