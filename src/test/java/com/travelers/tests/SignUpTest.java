package com.travelers.tests;

import com.aventstack.extentreports.ExtentTest;
import com.travelers.pages.HomePage;
import com.travelers.pages.LoggedUserPage;
import com.travelers.pages.SignUp;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.io.IOException;
import java.util.List;

public class SignUpTest extends BaseSeleniumTest {

    @Test
    public void signUpTest() throws IOException {
        ExtentTest test = reports.createTest("Sign Up Test");

        test.info("On the Home Page", getScreenshot());
        String lastName = "Testowa";
        int randomNumber = (int) (Math.random() * 10000000);
        String email = "testowy" + randomNumber + "@gmail.com";

        HomePage homePage = new HomePage(driver);
        homePage.openFormSignIn();

        SignUp signUp = new SignUp(driver);
        signUp.setFirstName("Joan");
        signUp.setLastName(lastName);
        signUp.setPhoneNumber("1234567");
        signUp.setEmail(email);
        signUp.setPassword("1234567");
        signUp.setConfirmPassword("1234567");
        signUp.clickConfirmPassword();

        LoggedUserPage loggedUserPage = new LoggedUserPage(driver);
        test.info("Filled form with correct data", getScreenshot());
        Assert.assertTrue(loggedUserPage.getHeadingText().contains(lastName));
        Assert.assertEquals(loggedUserPage.getHeadingText(), "Hi, Joan Testowa");
        test.info("Site after logging", getScreenshot());

        signUp.clickMyAccount();
        signUp.clickLogOut();
        test.info("After login out", getScreenshot());
    }

    @Test
    public void signUpTestWithoutData() throws IOException {
        ExtentTest test = reports.createTest("Sign Up Test");

        HomePage homePage = new HomePage(driver);
        homePage.openFormSignIn();

        SignUp signUp = new SignUp(driver);
        signUp.clickConfirmPassword();
        List<String> errors = signUp.takeErrors();

        test.info("On the Home Page", getScreenshot());
        test.info("Sign up with incorrect data", getScreenshot());


        SoftAssert softAssert = new SoftAssert();
        softAssert.assertTrue(errors.contains("The Email field is required."));
        softAssert.assertTrue(errors.contains("The Password field is required."));
        softAssert.assertTrue(errors.contains("The Password field is required."));
        softAssert.assertTrue(errors.contains("The First name field is required."));
        softAssert.assertTrue(errors.contains("The Last Name field is required."));
        softAssert.assertAll();
    }

    @Test
    public void signUpTestWithIncorrectEmail() throws IOException {
        ExtentTest test = reports.createTest("Sign Up Test");

        HomePage homePage = new HomePage(driver);
        homePage.openFormSignIn();

        test.info("On the Home Page", getScreenshot());
        String lastName = "Testowa";
        int randomNumber = (int) (Math.random() * 10000000);
        String email = "testowy" + randomNumber + "gmailcom";

        SignUp signUp = new SignUp(driver);
        signUp.setFirstName("Joan");
        signUp.setLastName(lastName);
        signUp.setPhoneNumber("1234567");
        signUp.setEmail(email);
        signUp.setPassword("1234567");
        signUp.setConfirmPassword("1234567");
        signUp.clickConfirmPassword();
        test.info("Sign up with incorrect email", getScreenshot());

        Assert.assertTrue(signUp.takeErrors().contains("The Email field must contain a valid email address."));
    }
}

